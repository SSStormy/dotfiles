# SSStormy's dotfiles

The dotfiles I use for my arch setup.

## Dependencies
###### Outdated
1. xorg
2. lemonbar
3. i3
4. zsh
  * oh-my-zsh
  * powerline9k theme
5. [vimrc](https://github.com/amix/vimrc)
6. twmn
7. rxvt-unicode
  * clipboard
8. Fonts
  * Powerline Fonts (Terminess/Terminus)
  * Font Awesome
  * OpenSans
9. Rofi
1. mpd
  * ncmpcpp
2. feh


###### License: WTFPL (these are just dotfiles who the fuck cares)

