#!/bin/bash
pactl load-module module-null-sink sink_name=GameAudio sink_properties=device.description=GameAudio
pactl load-module module-null-sink sink_name=Music     sink_properties=device.description=Music
pactl load-module module-null-sink sink_name=VoipAudio sink_properties=device.description=VoipAudio

pactl load-module module-combine-sink sink_name=Combined_GameAudio_And_Headset sink_properties=device.description=Combined_GameAudio_And_Headset slaves=alsa_output.pci-0000_00_14.2.analog-stereo,GameAudio
pactl load-module module-combine-sink sink_name=Combined_Music_And_Headset sink_properties=device.description=Combined_Music_And_Headset slaves=alsa_output.pci-0000_00_14.2.analog-stereo,Music
pactl load-module module-combine-sink sink_name=Combined_VoipAudio_And_Headset sink_properties=device.description=Combined_VoipAudio_And_Headset slaves=alsa_output.pci-0000_00_14.2.analog-stereo,VoipAudio


