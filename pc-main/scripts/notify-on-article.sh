#!/bin/bash

# Purpose: call notify-send if canto-remote tells us it has an unread article.

unread=`/usr/bin/canto-remote status`

if [[ $unread -gt 0 ]]; then
    /usr/bin/notify-send "RSS" "$unread unread articles."
fi
