export ZSH=/home/justas/.oh-my-zsh
export SYNC_DIR="/home/justas/sync"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/justas/bin:/home/justas/bin"


# TMUX
if which tmux >/dev/null 2>&1; then
	#if not inside a tmux session, and if no session is started, start a new session
	test -z "$TMUX" && tmux new-session
fi

ZSH_THEME="theunraveler"
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
plugins=(git colored-man-pages colorize copydir copyfile cp extract zsh-autosuggestions safe-paste)

source $ZSH/oh-my-zsh.sh

bindkey '^ ' autosuggest-execute

source ~/dotfiles/common/.shellrc

