execute pathogen#infect()

packloadall

" remap colons
nnoremap ; :
" line numbers
syntax on
set relativenumber
set number

" theme/colors
set t_Co=256
let g:rehash256 = 1
colorscheme molokai
set background=dark

" better window separator
set fillchars+=vert:│
hi VertSplit ctermbg=NONE guibg=NONE

" indentation
filetype plugin on
filetype plugin indent on
set shiftwidth=4
set tabstop=4
set expandtab
set softtabstop=4

" misc
set wildmode=longest,list,full
set wildmenu

set lazyredraw

set showmatch
set incsearch
set hlsearch

set cursorline

" bind to escape
inoremap jk <esc>

" store .swp in ~/.vim/tmp/
set directory^=$HOME/.vim/tmp//
set backupdir^=$HOME/.vim/tmp//

" disable arrow keys
noremap  <Up> ""
noremap  <Down> ""
noremap  <Left> ""
noremap  <Right> ""

nmap <silent> <S-k> :wincmd k<CR>
nmap <silent> <S-j> :wincmd j<CR>
nmap <silent> <S-h> :wincmd h<CR>
nmap <silent> <S-l> :wincmd l<CR>

" Use exec .vimrc in cwd
set secure
set exrc

" close if the nerdtree window is the only active window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" start NERDTree if no file was passed
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" show line numbers in the NERDTree
let g:NERDTreeShowLineNumbers=1

" c/cpp autocomplete
let g:clang_library_path='/usr/lib/'

" tab to select autocomplete
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>\<cr>" : "\<cr>"
