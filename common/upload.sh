#!/bin/bash

dir="http://213.226.180.166/stuff/"

rsync -avz "$@" srvrotw:~/public_html/

local body=""
clip=""

for f in "$@" ; do
    body+="$f\n"
    clip+="$dir$f "
done

notify-send "Ranger Upload" "$body"
echo "$clip" | xclip -selection clipboard
